import numpy as np
import cv2
import torch
import torchvision.transforms as transforms
import glob
from torch.utils.data import Dataset

def load_image(image_name):
    return cv2.cvtColor(cv2.imread(image_name),cv2.COLOR_BGR2RGB)


def split_image(image_raw,resize=True):
    image = image_raw.copy()

    if image.shape[1] < image.shape[0]:
        image = cv2.transpose(image)

    image_size = image.shape[0]
    n = image.shape[1]//image.shape[0]

    list_images = []

    if resize:
        for i in range(n):
            list_images.append(cv2.resize(image[:,image_size*i:image_size*(i+1),:],(464,464)))
        if (image.shape[1]%image.shape[0]!=0):
            list_images.append(cv2.resize(image[:,image.shape[1]-image_size:image.shape[1],:],(464,464)))
    else:
        for i in range(n):
            list_images.append(image[:,image_size*i:image_size*(i+1),:])
        if (image.shape[1]%image.shape[0]!=0):
            list_images.append(image[:,image.shape[1]-image_size:image.shape[1],:])
    
    return list_images


def image_to_tensor (image):
    transform = transforms.Compose([transforms.ToTensor()])
    tensor = transform(image)

    return tensor


class CSPSDataset():
    def __init__(self):
        self.file_list = glob.glob("./data/Plot*/**/*.jpeg",recursive=True)
        
        if len(self.file_list) < 2:
            raise("Error: too few files in the data path")
        
        sample = load_image(self.file_list[0])
        (self.image_height, self.image_width, _) = sample.shape
        print("Image Shape: ", sample.shape)

        # self.shape = sample.shape

        if self.image_height> self.image_width:
            self.samples_per_image = self.image_height// self.image_width
        else:
            self.samples_per_image = self.image_width// self.image_height


    def __len__ (self):

        return self.samples_per_image*len(self.file_list)


    def __getitem__(self, idx):
        idy = (idx//4)
        idz = idx-idy*4
        file_name = self.file_list[idy]
        path_list = file_name.split("/")
        
        if path_list[2] == "Plot Study 1 - 09152021":
            plot_study = "PS1"
        elif path_list[2] == "Plot Study 2 - 09202021":
            plot_study = "PS2"
        else:
            plot_study = "H"
        
        plot_n = path_list[3]
        label = plot_study + "-" + plot_n
        image = split_image(load_image(file_name))[idz]
        
        return image_to_tensor(image), label


    def sample_image(self, idx):
        idy = (idx//4)
        idz = idx-idy*4
        file_name = self.file_list[idy]
        path_list = file_name.split("/")
        
        image = split_image(load_image(file_name))[idz]
        
        return image