from mmdet.apis import init_detector, inference_detector
import mmcv
import glob

config_file = 'output/work_dir/config_maskrcnn.py'
checkpoint_file = 'output/work_dir/latest.pth'

model = init_detector(config_file, checkpoint_file, device='cuda:0')

images = glob.glob("data/COCO/images/*",recursive=True)

for i in range(20):
    result = inference_detector(model, images[i])
    model.show_result(images[i], result, out_file=f'output/result_{i}.jpg', score_thr=0.2)
