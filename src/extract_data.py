import glob
import shutil
import os
import json
import zipfile
from sklearn.model_selection import train_test_split

if __name__ == "__main__":

    zip_files = glob.glob("data/*.zip")
    extracted_folders = []
    for zip_file in zip_files:
        with zipfile.ZipFile(zip_file,"r") as zip_ref:
            unpack_folder = "data/"+zip_file.split("/")[-1].replace(".zip","")
            try:
                shutil.rmtree(unpack_folder)
            except:
                pass
            extracted_folders.append(unpack_folder)
            zip_ref.extractall(unpack_folder)

    try:
        shutil.rmtree("data/COCO")
    except:
        pass

    os.mkdir("data/COCO")
    os.mkdir("data/COCO/annotations")
    os.mkdir("data/COCO/images")

    all_images = glob.glob("data/*/images/*")
    for image in all_images:
        image_out = "data/COCO/images/"+image.split("/")[-1]
        shutil.move(image, image_out)
    
    out = {}
    out["licenses"] = [{"name": "","id": 0,"url": ""}]
    out["info"] ={"contributor": "","date_created": "","description": "","url": "","version": "","year": ""}
    out["categories"] = [{"id": 1,"name": "Grain","supercategory": ""},{"id": 2,"name": "Pod","supercategory": ""}]
    images = []
    image_id_offset = 0
    annotations = []
    annotation_id_offset = 0

    annotation_files = glob.glob("data/*/annotations/instances_default.json")
    for annotation_file in annotation_files:
        with open(annotation_file,"r") as f:
            j = json.load(f)
        
        for image in j["images"]:
            image["id"] += image_id_offset
        
        for annotation in j["annotations"]:
            annotation["id"] += annotation_id_offset
            annotation["image_id"] += image_id_offset
        
        image_id_offset+=len(j["images"])
        annotation_id_offset+=len(j["annotations"])

        if j["categories"][0]["name"].lower() != "grain":
            remap = {1:2,2:1}
            # print(j["annotations"][0]["category_id"])
            for annotation in j["annotations"]:
                annotation["category_id"] = remap[annotation["category_id"]]
            # print(j["annotations"][0]["category_id"])
        
        images.extend(j["images"])
        annotations.extend(j["annotations"])
    
    out["images"] = images
    out["annotations"] = annotations

    with open("data/COCO/annotations/instances_default.json","w") as f:
        json.dump(out, f, indent = 4)
    
    data_train, data_test = train_test_split(images, test_size=1/10, random_state=42)
    data_train, data_val = train_test_split(data_train, test_size=1/9, random_state=42)

    with open("data/COCO/annotations/instances_train.json","w") as f:
        out["images"] = data_train
        json.dump(out, f, indent = 4)
    with open("data/COCO/annotations/instances_test.json","w") as f:
        out["images"] = data_test
        json.dump(out, f, indent = 4)
    with open("data/COCO/annotations/instances_val.json","w") as f:
        out["images"] = data_val
        json.dump(out, f, indent = 4)
    
    for folder in extracted_folders:
        try:
            shutil.rmtree(folder)
        except:
            pass
    


