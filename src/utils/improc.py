import torch
import cv2
import numpy as np

def tensor_to_image(tensor):
    image = tensor.permute(1,2,0).detach().numpy()
    image = image*255
    image = image.astype(np.uint8)

    return cv2.cvtColor(image,cv2.COLOR_BGR2RGB)


def rotate_90(image):
    return cv2.rotate(image,cv2.ROTATE_90_CLOCKWISE)