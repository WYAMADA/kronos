#include "rclcpp/rclcpp.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "geometry_msgs/msg/pose.hpp"
#include "camera_interfaces/srv/trigger_camera.hpp"
#include <chrono>

using namespace std::chrono_literals;

class OdometrySubscriber : public rclcpp::Node
{
public:
    OdometrySubscriber()
    : Node("odometry_subscriber_node")
    {
        // Initialize the pose variable
        last_pose_.position.x = 0.0;
        last_pose_.position.y = 0.0;
        last_pose_.position.z = 0.0;

        // Subscribe to the odometry_filtered topic
        subscription_ = this->create_subscription<nav_msgs::msg::Odometry>(
            "/odometry/filtered", 10, std::bind(&OdometrySubscriber::topic_callback, this, std::placeholders::_1));
        client_ = this->create_client<camera_interfaces::srv::TriggerCamera>("head_camera/trigger_camera");
    }

private:
    void topic_callback(const nav_msgs::msg::Odometry::SharedPtr msg)
    {
        // Calculate the Euclidean distance between the current pose and the previous pose
        double distance = std::sqrt(std::pow(msg->pose.pose.position.x - last_pose_.position.x, 2) +
                                std::pow(msg->pose.pose.position.y - last_pose_.position.y, 2) +
                                std::pow(msg->pose.pose.position.z - last_pose_.position.z, 2));

        // If the distance is bigger than 1, update the pose
        if(distance > 1.0)
        {
            trigger();
            last_pose_ = msg->pose.pose;
            RCLCPP_INFO(this->get_logger(), "Updated Pose: %.2f, %.2f, %.2f", last_pose_.position.x, last_pose_.position.y, last_pose_.position.z);
        }
    }

    void trigger(){
        RCLCPP_INFO(this->get_logger(), "Triggering camera");
        auto request = std::make_shared<camera_interfaces::srv::TriggerCamera::Request>();
        while (!client_->wait_for_service(1s)) {
            if (!rclcpp::ok()) {
                RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
                return;
            }
            RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
        }

        auto result = client_->async_send_request(request);
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Service called!");
    }

    rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subscription_;
    rclcpp::Client<camera_interfaces::srv::TriggerCamera>::SharedPtr client_;
    geometry_msgs::msg::Pose last_pose_;
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<OdometrySubscriber>());
    rclcpp::shutdown();
    return 0;
}
