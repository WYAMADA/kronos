#include <librealsense2/rs.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <opencv2/opencv.hpp>
#include <nlohmann/json.hpp>
#include "camera_rs435.hpp"
#include "camera_interfaces/srv/trigger_camera.hpp"
#include "rclcpp/rclcpp.hpp"
#include <functional>

void camera_rs435::camera_callback(const std::shared_ptr<camera_interfaces::srv::TriggerCamera::Request> request,
          std::shared_ptr<camera_interfaces::srv::TriggerCamera::Response>      response){
    response->success = false;
    this->save_images();
    response->success = true;
}

void camera_rs435::color_callback(const std::shared_ptr<camera_interfaces::srv::TriggerCamera::Request> request,
          std::shared_ptr<camera_interfaces::srv::TriggerCamera::Response>      response){
    response->success = false;
    this->save_color();
    response->success = true;
}

void camera_rs435::infra_callback(const std::shared_ptr<camera_interfaces::srv::TriggerCamera::Request> request,
          std::shared_ptr<camera_interfaces::srv::TriggerCamera::Response>      response){
    response->success = false;
    this->save_infra();
    response->success = true;
}


int main (int argc, char **argv) {

    std::shared_ptr<camera_rs435> head = std::make_shared<camera_rs435>("/home/armadillo/kronos/cameras/configs/camera_d435i_head.json");

    rclcpp::init(argc, argv);
    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("head_camera_server"); 

    rclcpp::Service<camera_interfaces::srv::TriggerCamera>::SharedPtr service =
    node->create_service<camera_interfaces::srv::TriggerCamera>("head_camera/trigger_camera", std::bind(&camera_rs435::camera_callback, 
                                                                                            head,
                                                                                            std::placeholders::_1,
                                                                                            std::placeholders::_2));
    rclcpp::Service<camera_interfaces::srv::TriggerCamera>::SharedPtr service_color =
    node->create_service<camera_interfaces::srv::TriggerCamera>("head_camera/color_trigger_camera", std::bind(&camera_rs435::color_callback, 
                                                                                            head,
                                                                                            std::placeholders::_1,
                                                                                            std::placeholders::_2));
    rclcpp::Service<camera_interfaces::srv::TriggerCamera>::SharedPtr service_infra =
    node->create_service<camera_interfaces::srv::TriggerCamera>("head_camera/infra_trigger_camera", std::bind(&camera_rs435::infra_callback, 
                                                                                            head,
                                                                                            std::placeholders::_1,
                                                                                            std::placeholders::_2));
                    
    RCLCPP_INFO(rclcpp::get_logger("trigger_camera"), "Ready to take photos");

    rclcpp::spin(node);
    rclcpp::shutdown();
    // head->save_images();

    return 0;
}