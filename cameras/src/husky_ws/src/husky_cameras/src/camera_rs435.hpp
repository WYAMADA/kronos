#include <librealsense2/rs.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <opencv2/opencv.hpp>
#include <nlohmann/json.hpp>
#include <chrono>
#include <thread>
#include "camera_interfaces/srv/trigger_camera.hpp"
#include <ctime>
#include <sys/stat.h>
#include <sstream>

class camera_rs435
{
    private:
        int height;
        int width;
        int fps;
        bool enable_rgb;
        bool enable_infra1;
        bool enable_infra2;
        std::string name;
        std::string model;
        std::string serial_no;
        std::string cfg_file;
        rs2::pipeline pipe;
        rs2::config cfg;
        std::string out_folder;
        std::size_t count_rgb=0;
        std::size_t count_infra=0;
    public:
        camera_rs435(std::string);
        ~camera_rs435();
        bool save_images();
        bool save_color();
        bool save_infra();
        void camera_callback(const std::shared_ptr<camera_interfaces::srv::TriggerCamera::Request> request,
          std::shared_ptr<camera_interfaces::srv::TriggerCamera::Response>      response);
        void color_callback(const std::shared_ptr<camera_interfaces::srv::TriggerCamera::Request> request,
          std::shared_ptr<camera_interfaces::srv::TriggerCamera::Response>      response);
        void infra_callback(const std::shared_ptr<camera_interfaces::srv::TriggerCamera::Request> request,
          std::shared_ptr<camera_interfaces::srv::TriggerCamera::Response>      response);
};

camera_rs435::camera_rs435(std::string file){
    // std::cout << "Loading camera" << std::endl;
    cfg_file = file;
    std::ifstream ifs(cfg_file);
    nlohmann::json json_cfg = nlohmann::json::parse(ifs);
    if(json_cfg.contains("name"))
        name = json_cfg["name"];
    else
        name = "generic_camera";
    if(json_cfg.contains("camera_model"))
        model = json_cfg["camera_model"];
    if(json_cfg.contains("serial_no")){
        serial_no = json_cfg["serial_no"];
        cfg.enable_device(serial_no);
    }
    if(json_cfg.contains("height"))
        height = json_cfg["height"];
    if(json_cfg.contains("width"))
        width = json_cfg["width"];
    if(json_cfg.contains("fps"))
        fps = json_cfg["fps"];
    if(json_cfg.contains("enable_rgb"))
        enable_rgb = json_cfg["enable_rgb"];
    if(json_cfg.contains("enable_infra1"))
        enable_infra1 = json_cfg["enable_infra1"];
    if(json_cfg.contains("enable_infra2"))
        enable_infra2 = json_cfg["enable_infra2"];
    if(enable_rgb)
        cfg.enable_stream(RS2_STREAM_COLOR, width, height, RS2_FORMAT_BGR8, fps);
    if(enable_infra1)
        cfg.enable_stream(RS2_STREAM_INFRARED, 1, width, height, RS2_FORMAT_Y8, fps);
    if(enable_infra2)
        cfg.enable_stream(RS2_STREAM_INFRARED, 2, width, height, RS2_FORMAT_Y8, fps);
    
    // ctx = rs.context()
    // devices = ctx.query_devices()
    // for dev in devices:
    //     print("Resetting device")
    //     dev.hardware_reset()
    
    // rs2::context ctx;
    // rs2::device dev = ctx.query_devices().front(); 
    // dev.hardware_reset();
    // rs2::device_hub hub(ctx);
    // dev = hub.wait_for_device();
    
    // for(std::uint32_t i=0; i<ctx.query_devices().size() ; ++i){
    //     ctx.query_devices()[i].hardware_reset();
    //     rs2::device_hub hub(ctx);
    //     ctx.query_devices()[i] = hub.wait_for_device();
    // }

    pipe.start(cfg);

    // Turning off the emmiter
    rs2::pipeline_profile selection = pipe.get_active_profile();
    rs2::device selected_device = selection.get_device();
    selected_device.hardware_reset();

    auto depth_sensor = selected_device.first<rs2::depth_sensor>();

    if (depth_sensor.supports(RS2_OPTION_EMITTER_ENABLED))
    {
        depth_sensor.set_option(RS2_OPTION_EMITTER_ENABLED, 1.f); // Enable emitter
        depth_sensor.set_option(RS2_OPTION_EMITTER_ENABLED, 0.f); // Disable emitter
    }
    if (depth_sensor.supports(RS2_OPTION_LASER_POWER))
    {
        // Query min and max values:
        auto range = depth_sensor.get_option_range(RS2_OPTION_LASER_POWER);
        depth_sensor.set_option(RS2_OPTION_LASER_POWER, range.max); // Set max power
        depth_sensor.set_option(RS2_OPTION_LASER_POWER, 0.f); // Disable laser
    }

    // One second to adjust for exposure
    std::cout << "Waiting for camera to get some exposure...";
    // std::this_thread::sleep_for(std::chrono::seconds(1));
    for (auto i = 0; i < 30; ++i) pipe.wait_for_frames();
    std::cout << "done!\n";

    time_t now = time(0);
    tm *gmtm = gmtime(&now);
    out_folder = asctime(gmtm);
    out_folder = name+"-"+out_folder;

    std::cout << "New folder: " << out_folder << std::endl;
    mkdir(("./"+out_folder).c_str(), 0777);

}

camera_rs435::~camera_rs435(){
    pipe.stop();
}

bool camera_rs435::save_images(){
    // std::cout << "Saving images... ";
    
    std::ostringstream count_rgb_s;
    std::ostringstream count_infra_s;
    
    count_rgb_s << std::setw(5) << std::setfill('0') << count_rgb;
    count_infra_s << std::setw(5) << std::setfill('0') << count_infra;

    rs2::frameset frames = pipe.wait_for_frames(10000);

    if(enable_rgb){
        auto colored_frame = frames.get_color_frame();
        cv::Mat dMat_color = cv::Mat(cv::Size(width, height), CV_8UC3, (void*)colored_frame.get_data());
        // cv::cvtColor(dMat_color, dMat_color, cv::COLOR_BGR2RGB);
        // cv::imwrite( "color.png", dMat_color );
        cv::imwrite( ("./"+out_folder+"/"+count_rgb_s.str()+"_color.jpeg").c_str(), dMat_color );
        }
    if(enable_infra1) {
        auto ir_frame_left = frames.get_infrared_frame(1);
        cv::Mat dMat_left = cv::Mat(cv::Size(width, height), CV_8UC1, (void*)ir_frame_left.get_data());
        // cv::imwrite( "irLeft.png", dMat_left );
        cv::imwrite( ("./"+out_folder+"/"+count_infra_s.str()+"_irl.jpeg").c_str(), dMat_left );
        }
    if(enable_infra2){
        auto ir_frame_right = frames.get_infrared_frame(2);
        cv::Mat dMat_right = cv::Mat(cv::Size(width, height), CV_8UC1, (void*)ir_frame_right.get_data());
        // cv::imwrite( "irRight.png", dMat_right );
        cv::imwrite( ("./"+out_folder+"/"+count_infra_s.str()+"_irr.jpeg").c_str(), dMat_right );
        }
    count_rgb++;
    count_infra++;
    std::cout << "Count RGB: " << count_rgb_s.str() << std::endl;
    std::cout << "Count Infra: " << count_infra_s.str() << std::endl;
    // std::cout << "Done!\n";
    return true;
}

bool camera_rs435::save_color(){
    // std::cout << "Saving images... ";
    
    std::ostringstream count_s;
    
    count_s << std::setw(5) << std::setfill('0') << count_rgb;

    rs2::frameset frames = pipe.wait_for_frames();

    if(enable_rgb){
        auto colored_frame = frames.get_color_frame();
        cv::Mat dMat_color = cv::Mat(cv::Size(width, height), CV_8UC3, (void*)colored_frame.get_data());
        // cv::cvtColor(dMat_color, dMat_color, cv::COLOR_BGR2RGB);
        // cv::imwrite( "color.png", dMat_color );
        cv::imwrite( ("./"+out_folder+"/"+count_s.str()+"_color.jpeg").c_str(), dMat_color );
        }
    count_rgb++;
    std::cout << "Count RGB: " << count_s.str() << std::endl;
    // std::cout << "Done!\n";
    return true;
}

bool camera_rs435::save_infra(){
    // std::cout << "Saving images... ";
    
    std::ostringstream count_s;
    
    count_s << std::setw(5) << std::setfill('0') << count_infra;

    rs2::frameset frames = pipe.wait_for_frames();
    if(enable_infra1) {
        auto ir_frame_left = frames.get_infrared_frame(1);
        cv::Mat dMat_left = cv::Mat(cv::Size(width, height), CV_8UC1, (void*)ir_frame_left.get_data());
        // cv::imwrite( "irLeft.png", dMat_left );
        cv::imwrite( ("./"+out_folder+"/"+count_s.str()+"_irl.jpeg").c_str(), dMat_left );
        }
    if(enable_infra2){
        auto ir_frame_right = frames.get_infrared_frame(2);
        cv::Mat dMat_right = cv::Mat(cv::Size(width, height), CV_8UC1, (void*)ir_frame_right.get_data());
        // cv::imwrite( "irRight.png", dMat_right );
        cv::imwrite( ("./"+out_folder+"/"+count_s.str()+"_irr.jpeg").c_str(), dMat_right );
        }
    count_infra++;
    std::cout << "Count Infra: " << count_s.str() << std::endl;
    // std::cout << "Done!\n";
    return true;
}