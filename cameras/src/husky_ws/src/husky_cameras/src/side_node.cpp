#include <librealsense2/rs.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <opencv2/opencv.hpp>
#include <nlohmann/json.hpp>
#include "camera_rs435.hpp"
#include "camera_interfaces/srv/trigger_camera.hpp"
#include "rclcpp/rclcpp.hpp"
#include <functional>

void camera_rs435::camera_callback(const std::shared_ptr<camera_interfaces::srv::TriggerCamera::Request> request,
          std::shared_ptr<camera_interfaces::srv::TriggerCamera::Response>      response){
    response->success = false;
    this->save_images();
    std::cout << "Save image triggered" <<std::endl;
    response->success = true;
}

void camera_rs435::color_callback(const std::shared_ptr<camera_interfaces::srv::TriggerCamera::Request> request,
          std::shared_ptr<camera_interfaces::srv::TriggerCamera::Response>      response){
    response->success = false;
    this->save_color();
    response->success = true;
}

void camera_rs435::infra_callback(const std::shared_ptr<camera_interfaces::srv::TriggerCamera::Request> request,
          std::shared_ptr<camera_interfaces::srv::TriggerCamera::Response>      response){
    response->success = false;
    this->save_infra();
    response->success = true;
}


int main (int argc, char **argv) {

    std::shared_ptr<camera_rs435> side = std::make_shared<camera_rs435>("/home/armadillo/kronos/cameras/configs/camera_d435i_side.json");

    rclcpp::init(argc, argv);
    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("side_camera_server"); 

    rclcpp::Service<camera_interfaces::srv::TriggerCamera>::SharedPtr service =
    node->create_service<camera_interfaces::srv::TriggerCamera>("side_camera/trigger_camera", std::bind(&camera_rs435::camera_callback, 
                                                                                            side,
                                                                                            std::placeholders::_1,
                                                                                            std::placeholders::_2));
    rclcpp::Service<camera_interfaces::srv::TriggerCamera>::SharedPtr service_color =
    node->create_service<camera_interfaces::srv::TriggerCamera>("side_camera/color_trigger_camera", std::bind(&camera_rs435::color_callback, 
                                                                                            side,
                                                                                            std::placeholders::_1,
                                                                                            std::placeholders::_2));
    rclcpp::Service<camera_interfaces::srv::TriggerCamera>::SharedPtr service_infra =
    node->create_service<camera_interfaces::srv::TriggerCamera>("side_camera/infra_trigger_camera", std::bind(&camera_rs435::infra_callback, 
                                                                                            side,
                                                                                            std::placeholders::_1,
                                                                                            std::placeholders::_2));
                    
    RCLCPP_INFO(rclcpp::get_logger("trigger_camera"), "Ready to take photos");

    rclcpp::spin(node);
    rclcpp::shutdown();
    // side->save_images();

    return 0;
}