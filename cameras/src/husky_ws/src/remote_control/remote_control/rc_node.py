import rclpy
from rclpy.node import Node
from camera_interfaces.srv import TriggerCamera

from std_msgs.msg import String
from geometry_msgs.msg import Twist
import serial
import struct
import time
import threading

IBUS_LENGTH = 32
IBUS_CHANNELS = 14
channels_d = {}
# channels_d[0] = 1500
# channels_d[1] = 1500
toggle = False
old_channel_4 = 0

def parse_iBus(data):
    return struct.unpack('<' + 'H'*IBUS_CHANNELS, data[2:2+2*IBUS_CHANNELS])

def updating():
    global toggle
    global old_channel_4
    try:
        ser = serial.Serial('/dev/ttyTHS1', 115200, timeout=1)
        while True:
            data = ser.read(IBUS_LENGTH)
            if len(data) != IBUS_LENGTH:
                print("Failed to read data")
                continue
            channels = parse_iBus(data)
            for i, val in enumerate(channels):
                if i not in channels_d:
                    channels_d[i] = val
                    print("Channel {}: {}".format(i, val))
                else:
                    if abs(val - channels_d[i]) > 5:
                        channels_d[i] = val
                        print("Channel {}: {}".format(i, val))
            if channels_d[4]!=old_channel_4:
                old_channel_4=channels_d[4]
                toggle = True
    except serial.SerialException as e:
        print("Failed to open serial port: {}".format(e))

# try:
#     ser = serial.Serial('/dev/ttyTHS1', 115200, timeout=1)
#     while True:
#         data = ser.read(IBUS_LENGTH)
#         if len(data) != IBUS_LENGTH:
#             print("Failed to read data")
#             continue
#         channels = parse_iBus(data)
#         for i, val in enumerate(channels):
#             print("Channel {}: {}".format(i, val))
# except serial.SerialException as e:
#     print("Failed to open serial port: {}".format(e))

class RCPublisher(Node):

    def __init__(self):
        super().__init__('rc_publisher')
        self.publisher_ = self.create_publisher(Twist, '/husky_velocity_controller/cmd_vel_unstamped', 10)
        # self.publisher_ = self.create_publisher(Twist, '/joy_teleop/cmd_vel', 10)
        self.client_side = self.create_client(TriggerCamera,"/side_camera/trigger_camera")
        while not self.client_side.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('side service not available, waiting again...')
        self.client_head = self.create_client(TriggerCamera,"/head_camera/trigger_camera")
        while not self.client_head.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('head service not available, waiting again...')
        self.client_bird = self.create_client(TriggerCamera,"/bird_camera/trigger_camera")
        while not self.client_bird.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('bird service not available, waiting again...')
        self.req = TriggerCamera.Request()
        timer_period = 0.1  # seconds
        self.timer = self.create_timer(timer_period, self.loop)
        # self.i = 0
        # self.get_logger().info("Loading serial")
        # self.ser = serial.Serial('/dev/ttyTHS1', 115200, timeout=1)
        # self.get_logger().info("Loading serial complete")
        # self.linear_vel = 0.0
        # self.angular_vel = 0.0
        # self.channels_d = {}

        # self.get_logger().info("Starting loop")
        # self.loop()

    def loop(self):
        global toggle
        global old_channel_4
        msg = Twist()

        if(1 in channels_d):
            msg.linear.x = (channels_d[1]//5*5 - 1500)/500
            msg.angular.z = (channels_d[0]//5*5 - 1500)/500
            
            if(msg.linear.x >=-1 and msg.linear.x<=1 and msg.angular.z>=-1 and msg.angular.z<=1):
                self.publisher_.publish(msg)
            
            if(toggle):
                toggle = False
                self.get_logger().info("Calling Services")
                self.client_bird.call_async(self.req)
                self.client_head.call_async(self.req)
                self.client_side.call_async(self.req)
        #     time.sleep(0.1)
        # self.get_logger().info('Publishing: "%s"' % msg.data)
        # self.i += 1
        # return


def main(args=None):
    rclpy.init(args=args)

    minimal_publisher = RCPublisher()

    x = threading.Thread(target=updating)
    x.start()
    rclpy.spin(minimal_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()