import pyrealsense2 as rs
import numpy as np
import json
import cv2
import time

def main():

    with open('../configs/camera_head.json','r') as f:
        params = json.load(f)
    
    print("Camera Specifications:")
    print(params)

    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_device(params["serial_no"])
    config.disable_all_streams()

    if(params["infra"]):
        config.enable_stream(rs.stream.infrared, 1, params["infra_width"], params["infra_height"], rs.format.y8, params["infra_fps"])
        # config.enable_stream(rs.stream.infrared, 2, params["infra_width"], params["infra_height"], rs.format.y8, params["infra_fps"])
    if(params["rgb"]):
        config.enable_stream(rs.stream.color, params["rgb_width"], params["rgb_height"], rs.format.rgb8, params["rgb_fps"])

    ctx = rs.context()
    devices = ctx.query_devices()
    for dev in devices:
        print("Resetting device")
        dev.hardware_reset()

    print("starting pipeline...")
    pipeline.start(config)

    print("setting profiles...")
    profile = pipeline.get_active_profile()
    device = profile.get_device()
    depth_sensor = device.query_sensors()[0]
    depth_sensor.set_option(rs.option.emitter_enabled, False)

    # laser_pwr = depth_sensor.get_option(rs.option.laser_power)
    # print("laser power = ", laser_pwr)
    # laser_range = depth_sensor.get_option_range(rs.option.laser_power)
    # print("laser power range = " , laser_range.min , "~", laser_range.max)

    # emitter = depth_sensor.get_option(rs.option.emitter_enabled)
    # print("emmiter ",emitter)
    # if depth_sensor.supports(rs.option.emitter_enabled):
    #     depth_sensor.set_option(rs.option.emitter_enabled, 0)
    # emitter = depth_sensor.get_option(rs.option.emitter_enabled)
    # print("emmiter ",emitter)

    # time.sleep(3)

    # depth_sensor.set_option(rs.option.laser_power, 0)
    
    # laser_pwr = depth_sensor.get_option(rs.option.laser_power)
    # print("laser power = ", laser_pwr)
    
    # infrared_profile = rs.video_stream_profile(profile.get_stream(rs.stream.infrared, 2))
    # infrared_intrinsics = infrared_profile.get_intrinsics()
    i = 0
    # time.sleep(5)
    while(True):
        i +=1
        i -=1
        profile = pipeline.get_active_profile()
        device = profile.get_device()
        depth_sensor = device.query_sensors()[0]
        depth_sensor.set_option(rs.option.emitter_enabled, False)
        frames = pipeline.wait_for_frames()
        align = rs.align(rs.stream.color)
        aligned_frames = align.process(frames)
        color_frame = aligned_frames.get_color_frame()
        infrared_frame = aligned_frames.get_infrared_frame(1)
        # infrared_frame_2 = aligned_frames.get_infrared_frame(2)
        # color_frame = frames.get_color_frame()
        if not infrared_frame or not color_frame:
            continue
        # if not color_frame:
        #     continue
        print(type(color_frame))
        ir_image = np.asanyarray(infrared_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())

        ir_3channel_image = cv2.cvtColor(ir_image,cv2.COLOR_GRAY2BGR)
        color_image = cv2.cvtColor(color_image,cv2.COLOR_BGR2RGB)

        images = np.hstack((color_image, ir_3channel_image))
        
        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        # cv2.imshow('RealSense', color_image)
        cv2.imshow('RealSense', images)
        cv2.waitKey(1)

    return 0

if __name__ == "__main__":
    main()