import serial
import struct
import time

IBUS_LENGTH = 32
IBUS_CHANNELS = 14
channels_d = {}

IBUS_LENGTH = 32
IBUS_CHANNELS = 14

def parse_iBus(data):
    return struct.unpack('<' + 'H'*IBUS_CHANNELS, data[2:2+2*IBUS_CHANNELS])

# try:
#     ser = serial.Serial('/dev/ttyTHS1', 115200, timeout=1)
#     while True:
#         data = ser.read(IBUS_LENGTH)
#         if len(data) != IBUS_LENGTH:
#             print("Failed to read data")
#             continue
#         channels = parse_iBus(data)
#         for i, val in enumerate(channels):
#             print("Channel {}: {}".format(i, val))
# except serial.SerialException as e:
#     print("Failed to open serial port: {}".format(e))


class RCPublisher:
    def __init__(self):
        self.ser = serial.Serial('/dev/ttyTHS1', 115200, timeout=1)
        self.linear_vel = 0.0
        self.angular_vel = 0.0
        self.channels_d = {}
        print("Looping")
        
    def loop(self):
        while True:
            data = self.ser.read(IBUS_LENGTH)
            if len(data) != IBUS_LENGTH:
                print("Failed to read data")
                continue
            channels = parse_iBus(data)
            for i, val in enumerate(channels):
                if i not in self.channels_d:
                    print("Start")
                    self.channels_d[i] = val
                    print("Channel {}: {}".format(i, val))
                else:
                    print("Channel {}: {}".format(i, val))            
                    if (abs(val - self.channels_d[i]) > 10):
                        print("Update")
                        self.channels_d[i] = val
            time.sleep(0.1)

rc = RCPublisher()
rc.loop()