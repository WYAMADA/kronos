import serial
import struct

IBUS_LENGTH = 32
IBUS_CHANNELS = 14
channels_d = {}

def parse_iBus(data):
    return struct.unpack('<' + 'H'*IBUS_CHANNELS, data[2:2+2*IBUS_CHANNELS])

try:
    ser = serial.Serial('/dev/ttyTHS1', 115200, timeout=1)
    while True:
        data = ser.read(IBUS_LENGTH)
        if len(data) != IBUS_LENGTH:
            print("Failed to read data")
            continue
        channels = parse_iBus(data)
        for i, val in enumerate(channels):
            if i not in channels_d:
                channels_d[i] = val
                print("Channel {}: {}".format(i, val))
            else:
                if abs(val - channels_d[i]) > 5:
                    channels_d[i] = val
                    print("Channel {}: {}".format(i, val))
                    
except serial.SerialException as e:
    print("Failed to open serial port: {}".format(e))