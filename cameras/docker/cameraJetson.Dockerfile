FROM nvidiajetson/l4t-ros2-foxy-pytorch:r32.5

SHELL ["/bin/bash", "-c"]

# install husky dependencies 
ENV DEBIAN_FRONTEND=noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt update && apt install -y wget
# RUN wget https://packages.clearpathrobotics.com/public.key -O - | sudo apt-key add -
# RUN sh -c 'echo "deb https://packages.clearpathrobotics.com/stable/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/clearpath-latest.list'
# RUN apt update && sudo apt-get install -y ros-foxy-husky-desktop ros-foxy-husky-simulator ros-foxy-husky-robot
# RUN wget -c https://raw.githubusercontent.com/clearpathrobotics/ros_computer_setup/main/install.sh \
#     && bash install.sh -d nano -r husky -y



RUN apt-get update -y \
 && apt-get install -y \
    build-essential \
    cmake \
    git-all \
    software-properties-common

# install realsense cameras
RUN git clone https://github.com/IntelRealSense/librealsense.git
RUN sudo apt-get install -y git libssl-dev libusb-1.0-0-dev libudev-dev pkg-config libgtk-3-dev
RUN cd librealsense && ./scripts/setup_udev_rules.sh
RUN cd librealsense && mkdir build && cd build && cmake .. && make -j1 && make install

# install opencv
RUN apt update && apt install -y cmake make g++ wget unzip
RUN mkdir opencv
RUN cd opencv && wget -O opencv.zip https://github.com/opencv/opencv/archive/4.x.zip \
    && wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/4.x.zip \
    && unzip opencv.zip \
    && unzip opencv_contrib.zip
RUN cd opencv \
    && mkdir -p build && cd build \
    && cmake -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-4.x/modules ../opencv-4.x \
    && cmake --build . \
    && make -j4 \
    && make install

# install realsense 
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE
RUN add-apt-repository "deb https://librealsense.intel.com/Debian/apt-repo $(lsb_release -cs) main" -u
RUN apt update
RUN apt-get install -y librealsense2-utils
RUN apt-get install -y librealsense2-dev

# install json nlohmann
RUN git clone https://github.com/nlohmann/json.git
RUN cd json && mkdir build && cd build && cmake .. && make && make install
