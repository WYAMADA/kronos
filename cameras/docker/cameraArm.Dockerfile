FROM arm64v8/ros:foxy

SHELL ["/bin/bash", "-c"]

ENV DEBIAN_FRONTEND=noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt update && apt install -y wget
RUN wget https://packages.clearpathrobotics.com/public.key -O - | sudo apt-key add -
RUN sh -c 'echo "deb https://packages.clearpathrobotics.com/stable/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/clearpath-latest.list'
RUN apt update && sudo apt-get install -y ros-foxy-husky-desktop ros-foxy-husky-simulator ros-foxy-husky-robot

RUN apt-get update -y \
 && apt-get install -y \
    build-essential \
    cmake \
    git-all \
    software-properties-common

RUN git clone https://github.com/IntelRealSense/librealsense.git
RUN sudo apt-get install -y git libssl-dev libusb-1.0-0-dev libudev-dev pkg-config libgtk-3-dev
RUN cd librealsense && ./scripts/setup_udev_rules.sh
RUN cd librealsense && mkdir build && cd build && cmake .. && make -j1 && make install

RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt install curl # if you haven't already installed curl
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add 
RUN apt update
RUN apt install -y ros-foxy-realsense2-*
