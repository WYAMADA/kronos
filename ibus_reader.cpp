#include <iostream>
#include <fstream>
#include <vector>

#define IBUS_LENGTH 32
#define IBUS_CHANNELS 14

std::vector<int> parse_iBus(std::vector<uint8_t> buffer) {
    std::vector<int> channels(IBUS_CHANNELS);
    for(int i = 0; i < IBUS_CHANNELS; i++) {
        channels[i] = buffer[2 + i * 2] + (buffer[3 + i * 2] << 8);
    }
    return channels;
}

int main() {
    std::ifstream serialPort;
    serialPort.open("/dev/ttyTHS1", std::ios::binary);

    if(!serialPort.is_open()) {
        std::cout << "Failed to open serial port" << std::endl;
        return -1;
    }

    std::vector<uint8_t> buffer(IBUS_LENGTH);
    while(true) {
        serialPort.read(reinterpret_cast<char*>(buffer.data()), IBUS_LENGTH);
        // if(serialPort.gcount() != IBUS_LENGTH) {
        //     std::cout << "Failed to read data" << std::endl;
        //     continue;
        // }

        std::vector<int> channels = parse_iBus(buffer);
        for(int i = 0; i < channels.size(); i++) {
            std::cout << "Channel " << i << ": " << channels[i] << std::endl;
        }
    }

    serialPort.close();
    return 0;
}